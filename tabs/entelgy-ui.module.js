(function () {

  // normalente se guardan estos componentes de interfaz en algo tipo UI corporativo...

  angular
    .module('entelgy-ui', [])

    // Tabs
    .directive('euiTabs', function () {
      return {
        restrict: 'E',
        transclude: true, // tiene cosas dentro
        scope: true,
        template: [
          // esto no lo hagais así...
          '<div>',
            '<div class="flex">',
              '<div ng-repeat="tab in vm.tabs"',
                'ng-click="vm.selectTab($index)"',
                'class="p-2 mr-1 bg-grey-lightest border-l border-r border-t"',
                'ng-class="{ \'opacity-25\': $index !== vm.currentIndex }">',
                '<i ng-show="tab.scope.hasError === false" class="ion-checkmark-round text-green"></i>',
                '<i ng-show="tab.scope.hasError === true" class="ion-android-alert text-red"></i>',
                '&nbsp;{{tab.scope.label}}',
              '</div>',
            '</div>',
            // al ser la directiva de tipo transclude: true requiere un elemento donde hacer el transclude (ng-transclude)
            '<div class="border" ng-transclude></div>',
          '</div>'
        ].join(''),
        controllerAs: 'vm',
        controller: function () {
          var vm = this;
          vm.tabs = [];
          vm.currentIndex = 0;
          vm.registerTab = function (tab) {
            vm.tabs.push(tab);
            vm.selectTab(vm.currentIndex);
          };
          vm.selectTab = function (tabIndex) {
            vm.currentIndex = tabIndex;
            vm.tabs.forEach(function (tab, index) {
              tab.el[0].style.display = index === tabIndex ? 'block' : 'none'
            });
          };
        }
      };
    })

    // Tab
    .directive('euiTab', function () {
      return {
        restrict: 'E',
        require: '^euiTabs', // requiere un controlador de Tabs superior para gestionar su visibilidad
        scope: {
          label: '@',
          hasError: '='
        },
        link: function (scope, iElement, iAttributes, euiTabsController) {
          // esta funcino se llama seguido de estar preparado el elmeneto y todas sus cosas
          // registro el tab en el controlador superior para que lo gestione el
          euiTabsController.registerTab({el: iElement, scope: scope});
          // esta directiva en realidad no hace nada
        }
      };
    });

}());