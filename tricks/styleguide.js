
// Hay tres formas diferentes de injectar dependencias que conozca:
// la mas recomendada que sepa yo (super clara):

function FormController (factoryA, $scope) {...}
FormController.$inject = ['factoryA', '$scope']
angular.module('app', [])
  .controller('FormController', FormController);


// la super cómoda (que uso en estas demos):
angular.module('app', ['validators'])
  .controller('FormController', ['$scope', function ($scope) {...}]);


// la que nunca deberíais usar!! podría provocar que al minificar el javascript se rompa todo.
// podeis mirar la directiva ng-strict-di para saber mas sobre el tema:
// https://docs.angularjs.org/api/ng/directive/ngApp
angular.module('app', ['validators'])
  .controller('FormController', function ($scope) {...});

